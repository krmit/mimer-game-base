"use strict";
const assetsPath = "../../../../mimer-assets-pre/";

export class IdleBasicScene extends Phaser.Scene {

        constructor (config)
        {
            super(config);
            Phaser.Scene.call(this, { key: "IdleBasicScene", active: true });
            this.player;
            this.pen;
            this.gameOn=true;
            this.score=0;
            this.scoreMsg="Score: ";
            this.scoreText;
        }

        preload ()
        {
			 this.load.image('goal', assetsPath + 'icons/arrow-in-circle-64x64.png');
        }

        create ()
        {
             this.pen = this.add.graphics();
             let goal = this.add.sprite(400, 300, 'goal').setInteractive();
             this.scoreText = this.add.text(this.step, this.step,this.scoreMsg+this.score, { fontSize: '32px', fill: '#000' });

             goal.on('pointerdown', () => {
                 this.score++;
                 goal.rotation -= 0.04;
             })
        }

        update()
        {
			if(this.gameOn) {
				 this.scoreText.setText(this.scoreMsg+this.score); 
				
		}
  
    }
}
