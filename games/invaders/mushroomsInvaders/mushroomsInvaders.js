"use strict";
import {TitleScene} from "../../../..//mimer-game-lib/scenes/game.js";

const assetsPath = "../../../../mimer-assets-pre/";
let score= 0;
let life= 3;
let lifeMsg= "Life: ";
let scoreMsg= "Score: ";

let scoreText= null;
let scoreTexts= null;

let title_scene = new TitleScene({
  time: 1000,
  titleText: "Mushrooms \nInvder",
  nextScene: "invaderScene"
});


class InvaderScene extends Phaser.Scene {

        constructor (config)
        {
            super(config);
            Phaser.Scene.call(this, { key: "invaderScene", active: true });
            this.gameOn=true;
            this.my_buttons = [];
            this.step=6;
            this.player=null;
            this.spacebar;
            this.invaders = null;

        }

        preload() {
			this.load.spritesheet('mushrooms', assetsPath + 'icons/mushrooms-diffrent-color-128x128-7x3.png', { frameWidth: 128, frameHeight: 128});
			this.load.image('player', assetsPath + 'ships/raven-128x128.png');
	        this.load.spritesheet('shot', assetsPath + 'particel/many-colors-400x400-9x1.png', { frameWidth: 400, frameHeight: 400});
			this.load.image('wall', assetsPath + 'paddles/paddle-yellow-256x64.png');

		}

        create ()
        {
            //For the player

			this.player = this.physics.add.image(400, 550, 'player');
			this.player.setScale(0.5).setRotation(-Math.PI/2);
            this.player.setScale(0.5);

			this.cursors = this.input.keyboard.createCursorKeys();
			this.spacebar = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);

			// Create a bullet


			// For the wall
			this.wall = this.physics.add.image(870, 300, 'wall');
			this.wall = this.physics.add.image(880, 300, 'wall');
			this.wall = this.physics.add.image(890, 300, 'wall');

            this.wall.setScale(2.7);


			//For the invader

          	this.invaders = this.physics.add.group({ key: 'mushrooms', frame: 5, repeat: 1000, setXY: { x: -60100, y: 100, stepX: 60}, setScale: { x: 0.40} });
            this.invaders .setVelocityX(90);


            scoreText = this.add.text(16, 16, 'Score: 0', { fontSize: '32px', fill: '#ffffff' });
			scoreTexts = this.add.text(250, 16, 'Life: 3', { fontSize: '32px', fill: '#ffffff' });

         }

        update()
        {

		  	this.physics.add.overlap(this.invaders, this.wall, this.hits, this.life);

          if(this.gameOn) {
	         if (this.cursors.left.isDown)
             {
                 this.player.x-=this.step;
             }
             else if (this.cursors.right.isDown)
             {
                 this.player.x+=this.step;
             }
             else if (Phaser.Input.Keyboard.JustDown(this.spacebar))
             {
                 this.shoot();

             }

             if (this.invaders.x > -60100)
             {
			     life -= 1;
				scoreTexts.setText(lifeMsg + life);


             }


             if (score > 10)
             {

				 this.invaders .setVelocityX(120);

             }

			 if (score > 20)
			 {
			 this.invaders .setVelocityX(150);

			 }

			 if (score > 30)
			 {
			 this.invaders .setVelocityX(180);

			 }


			 if (score > 50)
			 {
			 this.invaders .setVelocityX(210);

			 }

			 if (score > 100)
			 {
			 this.invaders .setVelocityX(300);

			 }

			 if (life === 0)
			 {

			 scoreText = this.add.text(80, 300, 'Thanos granted you a score of: ' + score, { fontSize: '32px', fill: '#ff0000' });





			 }

			  if (life === -1)
			 {
			 game.destroy();
			 }


			 if (this.player.x >850)
			 {
			 this.step=0;
			 }

			 if (this.player.x <-50)
			 {
			 this.step=0;
			 }
		}
		}




		shoot()
        {






            let bullet = this.physics.add.image(this.player.x, this.player.y, 'shot');
            bullet.body.velocity.y = -400;
            bullet.setScale(0.1);
            this.physics.add.overlap(bullet, this.invaders, this.hit, this.score);













	    }

	    hit(bullet, invader)
        {
            console.log("hit");
            bullet.destroy();
            invader.destroy();
        	score += 1;
			scoreTexts.setText(lifeMsg + life);
            scoreText.setText(scoreMsg + score);

        }

		hits(invader, wall)
        {
            console.log("jebaited!");
            wall.destroy();
			life -= 1;
			scoreTexts.setText(lifeMsg + life);

        }




}

export const config = {
    type: Phaser.WEBGL,
    width: 800,
    height: 600,
    backgroundColor: "#000",
    parent: "phaser-demo",
    scene: [title_scene, InvaderScene],
    physics: {
    default: 'arcade',
    arcade: {
        debug: false,
        gravity: { y: 0 }
    }
}
};
